<?php
if (!is_user_logged_in()) {
    wp_redirect(get_permalink(get_page_by_path('entrar-cadastrar')));
}
get_header();
?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container mb-5">

        <div class="row">
            <div class="col-lg-3 mt-4 mt-lg-0">

                <div class="d-flex justify-content-center mb-4">
                    <div class="profile-image-outer-container">
                        <div class="profile-image-inner-container bg-color-primary">
                            <?php
                            $foto = get_field('foto', 'user_' . $current_user->ID);
                            if ($foto) {
                                ?>
                                <img src="<?php echo $foto ?>">
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar.jpg">
                                <?php
                            }
                            ?>
                            <span class="profile-image-button bg-color-dark">
                                <i class="fas fa-camera text-light"></i>
                            </span>
                        </div>
                        <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form img_ajax">
                            <input type="file" id="fotoperfil" name="foto" class="profile-image-input">
                            <input type="hidden" name="action" value="salvarimagem">
                        </form>
                    </div>
                </div>

                <aside class="sidebar mt-2" id="sidebar">
                    <ul class="nav nav-list flex-column mb-5">
                        <li class="nav-item"><a class="nav-link" href="#dadosdeacesso">Dados de acesso</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contato">Contato</a></li>
                        <li class="nav-item"><a class="nav-link" href="#redessociais">Redes Sociais</a></li>
                        <li class="nav-item"><a class="nav-link" href="#atuacao">Atuação</a></li>
                        <li class="nav-item"><a class="nav-link" href="#ajuda">Ajuda</a></li>
                    </ul>
                </aside>

            </div>
            <div class="col-lg-9">

                <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form">
                    <div class="overflow-hidden mb-1">
                        <h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Meu</strong> Perfil</h2>
                    </div>
                    <div class="overflow-hidden mb-4 pb-3">
                        <p class="mb-0">Edite seu perfil e selecione o tipo de ajuda que você pode dar.</p>
                    </div>

                    <div class="text-right"><button type="submit" class="btn btn-success">Saltar</button></div>
                    <hr>
                    <h3 id="dadosdeacesso">Dados de acesso</h3>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Nome completo <span class="color-red">*</span></label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                </div>
                                <input class="form-control" type="text" name="user_firstname" value="<?php echo $current_user->user_firstname ?>" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">E-mail <span class="color-red">*</span></label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input class="form-control" type="email" name="user_email" value="<?php echo $current_user->user_email ?>" required="">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h3 id="contato">Contato</h3>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">WhatsApp</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-whatsapp"></i></span>
                                </div>
                                <input class="form-control" type="text" name="whatsapp" value="<?php the_field('whatsapp', 'user_' . $current_user->ID) ?>" placeholder="Insira seu WhatsApp">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Skype</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-skype"></i></span>
                                </div>
                                <input class="form-control" type="text" name="skype" value="<?php the_field('skype', 'user_' . $current_user->ID) ?>" placeholder="Insira seu Skype">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Telefone</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
                                </div>
                                <input class="form-control" type="text" name="telefone" value="<?php the_field('telefone', 'user_' . $current_user->ID) ?>" placeholder="Insira seu Telefone">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h3 id="redessociais">Redes Sociais</h3>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Facebook</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-facebook"></i></span>
                                </div>
                                <input class="form-control" type="url" name="facebook" value="<?php the_field('facebook', 'user_' . $current_user->ID) ?>" placeholder="Insira a URL completa do seu Facebook">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Instagram</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-instagram"></i></span>
                                </div>
                                <input class="form-control" type="url" name="instagram" value="<?php the_field('instagram', 'user_' . $current_user->ID) ?>" placeholder="Insira a URL completa do seu Instagram">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">LinkedIn</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-linkedin"></i></span>
                                </div>
                                <input class="form-control" type="url" name="linkedin" value="<?php the_field('linkedin', 'user_' . $current_user->ID) ?>" placeholder="Insira a URL completa do seu LinkedIn">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h3 id="atuacao">Atuação</h3>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">CEP <span class="color-red">*</span></label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-globe-americas"></i></span>
                                </div>
                                <input class="form-control" type="text" name="cep" value="<?php the_field('cep', 'user_' . $current_user->ID) ?>" placeholder="Insira seu CEP" required="">
                            </div>
                            <small>Insira o CEP de onde irá ajudar as pessoas.</small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Raio de atuação <span class="color-red">*</span></label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-map-marker-alt"></i></span>
                                </div>
                                <select id="user_time_zone" name="atuacao" class="form-control" size="0" required="">
                                    <option value="">-Selecionar</option>
                                    <?php $atuacao = get_field('atuacao', 'user_' . $current_user->ID); ?>
                                    <option value="2"<?php echo ($atuacao == 2 ? ' selected=""' : '') ?>>Até 2km</option>
                                    <option value="5"<?php echo ($atuacao == 5 ? ' selected=""' : '') ?>>Até 5km</option>
                                    <option value="7"<?php echo ($atuacao == 7 ? ' selected=""' : '') ?>>Até 7km</option>
                                    <option value="10"<?php echo ($atuacao == 10 ? ' selected=""' : '') ?>>Até 10km</option>
                                    <option value="15"<?php echo ($atuacao == 15 ? ' selected=""' : '') ?>>Até 15km</option>
                                    <option value="20"<?php echo ($atuacao == 20 ? ' selected=""' : '') ?>>Até 20km</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="text-right"><button type="submit" class="btn btn-success">Saltar</button></div>
                    <input type="hidden" name="action" value="atualizarperfil">
                </form>
            </div>
        </div>


        <?php
        $ajudas = new WP_Query(array('post_type' => 'ajudas', 'posts_per_page' => '-1'));
        if ($ajudas->have_posts()) {
            ?>
            <hr>
            <h3 id="ajuda">Ajudas</h3>
            <p>Você pode ajudar as pessoas de várias formas, selecione como você pretende ajudar pessoas no grupo de risco clicando nos botões abaixo.</p>
            <div class="row">
                <?php
                $ajuda = get_field('ajuda', 'user_' . $current_user->ID);
                while ($ajudas->have_posts()) {
                    $ajudas->the_post();
                    ?>
                    <div class="col-md-6 col-lg-6 mb-3">
                        <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form_ajuda" id="ajudasform<?php the_id() ?>" data-ajudaid="<?php the_id() ?>">
                            <div class="card bg-color-grey">
                                <div class="card-body">
                                    <h4 class="m-0">
                                        <input type="checkbox" name="ajudastatus" class="ajudasbtn" data-ajudaid="<?php the_id() ?>" data-on="Ativo" data-off="Inativo" data-toggle="toggle" data-size="sm"<?php echo (in_array(get_the_id(), $ajuda) ? ' checked=""' : '') ?> id="ajudabtn<?php the_id(); ?>">
                                        <?php the_title(); ?>
                                    </h4>
                                    <span id="ajudaload<?php the_id() ?>"></span>
                                    <input type="hidden" name="action" value="salvarajuda">
                                    <input type="hidden" name="ajudaid" value="<?php the_id() ?>">
                                    <?php the_field('detalhes_da_ajuda') ?>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php
                }
                wp_reset_postdata();
                ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php get_footer() ?>