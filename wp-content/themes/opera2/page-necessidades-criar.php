<?php

function my_acf_prepare_field($field) {
    $field['label'] = "Qual sua necessidade?";
    return $field;
}

add_filter('acf/prepare_field/name=_post_title', 'my_acf_prepare_field');

function wpdocs_dequeue_script() {
    wp_dequeue_script('jquery.min.js');
}

add_action('wp_print_scripts', 'wpdocs_dequeue_script', 100);
acf_form_head();
get_header();
?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4 mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <?php
                    if (is_user_logged_in()) {
                        acf_form(array(
                            'id' => 'new-necessidades',
                            'post_id' => 'new_post',
                            'post_title' => true,
                            'html_submit_button' => '<button type="submit" class="btn btn-success">%s</button>',
                            'new_post' => array(
                                'post_type' => 'necessidades',
                                'post_status' => 'pending'
                            ),
                            'submit_value' => 'Cadastrar Necessidade',
                            'updated_message' => '<div class="alert alert-success">Sua necessidade foi cadastrada com sucesso. Nossa equipe irá avaliar e em breve deixaremos dis[pnível no sistema.</div>',
                        ));
                    } else {
                        ?>
                        <div class="alert alert-warning">Para cadastrar uma necessidade, crie uma conta ou faça o login.</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>