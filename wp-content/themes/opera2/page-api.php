<?php

header('Content-Type: application/json');
$action = filter_input(INPUT_POST, 'action');
if (!empty($action)) {
    $error = action_front($action);
} else {
    $error = array('status' => 'danger', 'message' => 'Ocorreu um erro durante a requisição. Por favor, tente novamente mais tarde.');
}

echo json_encode($error);
