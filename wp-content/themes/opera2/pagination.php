<?php
function wp_bs_pagination($pages = '', $range = 4) {
    $showitems = ($range * 2) + 1;
    global $paged;
    if (empty($paged))
        $paged = 1;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }
    if (1 != $pages) {
        echo '<br clear="all"><div class="text-center"><ul class="pagination pagination-lg">';
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link(1) . "'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a></li>";
        if ($paged > 1 && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link($paged - 1) . "' aria-label='Previous'><i class='fa fa-chevron-left'></i></a></li>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                echo ($paged == $i) ? "<li class='active'><a href='#'>" . $i . "</a>
                </li>" : "<li><a href='" . get_pagenum_link($i) . "'>" . $i . "</a></li>";
            }
        }

        if ($paged < $pages && $showitems < $pages)
            echo "<li><a href=\"" . get_pagenum_link($paged + 1) . "\"><i class='fa fa-chevron-right'></i></a></li>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<li><a href='" . get_pagenum_link($pages) . "'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
        echo "</ul></div>";
    }
}

if (function_exists("wp_bs_pagination")) {
    wp_bs_pagination();
}