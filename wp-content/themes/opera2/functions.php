<?php

error_reporting(E_ERROR | E_PARSE);
//Adicionando Core Opera2
include_once 'functions-opera2.php';
include_once 'functions-post-type.php';
include_once 'functions-actions.php';
include_once 'functions-fields.php';

//Removendo JavaScript padrão do Wordpress
function disable_embeds_init() {
    remove_action('wp_head', 'wp_oembed_add_host_js');
}

add_action('init', 'disable_embeds_init', 9999);

//Removendo links e metas do cabeçalho
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

//Adicionando suportes ao tema
function theme_setup() {
    add_theme_support('title-tag');
    load_theme_textdomain('game', get_template_directory() . '/languages');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));
}

add_action('after_setup_theme', 'theme_setup');

function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyCyhKpymEtdtp_7nN_090UlNm8s4g_R-QA');
}

add_action('acf/init', 'my_acf_init');

//Removendo páginas não usadas da adminisração e fazendo redirect do dashboard
function remove_menus() {
    remove_menu_page('index.php');                  //Dashboard
    remove_menu_page('jetpack');                    //Jetpack* 
    remove_menu_page('edit.php');                   //Posts
//    remove_menu_page('upload.php');                 //Media
//    remove_menu_page('edit.php?post_type=page');    //Pages
    remove_menu_page('edit-comments.php');          //Comments
//    remove_menu_page('themes.php');                 //Appearance
//    remove_menu_page('plugins.php');                //Plugins
//    remove_menu_page('users.php');                  //Users
//    remove_menu_page('tools.php');                  //Tools
//    remove_menu_page('options-general.php');        //Settings
}

add_action('admin_menu', 'remove_menus');

function admin_redirects() {
    global $pagenow;

    /* Redirect Customizer to Theme options */
    if ($pagenow == 'index.php') {
        wp_redirect(admin_url('/edit.php?post_type=page', 'http'), 301);
        exit;
    }
}

add_action('admin_init', 'admin_redirects');

//Adicionando Javascript e CSS
function wpdocs_scripts_method() {
    //CSS Opera2
    wp_enqueue_style('style.o2', get_stylesheet_directory_uri() . '/style.css');

    //CSS Template
    wp_enqueue_style('fonts.googleapis.css', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400');
    wp_enqueue_style('bootstrap.min.css', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap-4.3.1-dist/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap4-toggle.min.css', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap4-toggle-master/css/bootstrap4-toggle.min.css');
    wp_enqueue_style('all.min.css', get_stylesheet_directory_uri() . '/assets/vendor/fontawesome-free-5.13.0-web/css/all.min.css');
    wp_enqueue_style('animate.min.css', get_stylesheet_directory_uri() . '/assets/vendor/animate/animate.min.css');
    wp_enqueue_style('simple-line-icons.min.css', get_stylesheet_directory_uri() . '/assets/vendor/simple-line-icons/css/simple-line-icons.min.css');
    wp_enqueue_style('owl.carousel.min.css', get_stylesheet_directory_uri() . '/assets/vendor/owl.carousel/assets/owl.carousel.min.css');
    wp_enqueue_style('owl.theme.default.min.css', get_stylesheet_directory_uri() . '/assets/vendor/owl.carousel/assets/owl.theme.default.min.css');
    wp_enqueue_style('magnific-popup.min.css', get_stylesheet_directory_uri() . '/assets/vendor/magnific-popup/magnific-popup.min.css');
    wp_enqueue_style('theme.css', get_stylesheet_directory_uri() . '/assets/css/theme.css');
    wp_enqueue_style('theme-elements.css', get_stylesheet_directory_uri() . '/assets/css/theme-elements.css');
    wp_enqueue_style('theme-blog.css', get_stylesheet_directory_uri() . '/assets/css/theme-blog.css');
    wp_enqueue_style('settings.css', get_stylesheet_directory_uri() . '/assets/vendor/rs-plugin/css/settings.css');
    wp_enqueue_style('layers.css', get_stylesheet_directory_uri() . '/assets/vendor/rs-plugin/css/layers.css');
    wp_enqueue_style('navigation.css', get_stylesheet_directory_uri() . '/assets/vendor/rs-plugin/css/navigation.css');
    wp_enqueue_style('component.css', get_stylesheet_directory_uri() . '/assets/vendor/circle-flip-slideshow/css/component.css');
    wp_enqueue_style('default.css', get_stylesheet_directory_uri() . '/assets/css/skins/default.css');
    wp_enqueue_style('custom.css', get_stylesheet_directory_uri() . '/assets/css/custom.css');


    //Javascript Template
    wp_enqueue_script('modernizr.min.js', get_stylesheet_directory_uri() . '/assets/vendor/modernizr/modernizr.min.js', '', '', false);
    wp_enqueue_script('jquery.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery/jquery.min.js', '', '', true);
    wp_enqueue_script('jquery.appear.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.appear/jquery.appear.min.js', '', '', true);
    wp_enqueue_script('jquery.easing.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.easing/jquery.easing.min.js', '', '', true);
    wp_enqueue_script('jquery.cookie.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.cookie/jquery.cookie.min.js', '', '', true);
    wp_enqueue_script('popper.min.js', get_stylesheet_directory_uri() . '/assets/vendor/popper/umd/popper.min.js', '', '', true);
    wp_enqueue_script('bootstrap.min.js', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap-4.3.1-dist/js/bootstrap.min.js', '', '', true);
    wp_enqueue_script('bootstrap4-toggle.min.js', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap4-toggle-master/js/bootstrap4-toggle.min.js', '', '', true);
    wp_enqueue_script('common.min.js', get_stylesheet_directory_uri() . '/assets/vendor/common/common.min.js', '', '', true);
    wp_enqueue_script('jquery.validate.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.validation/jquery.validate.min.js', '', '', true);
    wp_enqueue_script('jquery.easypiechart.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js', '', '', true);
    wp_enqueue_script('jquery.gmap.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.gmap/jquery.gmap.min.js', '', '', true);
    wp_enqueue_script('jquery.lazyload.min.js', get_stylesheet_directory_uri() . '/assets/vendor/jquery.lazyload/jquery.lazyload.min.js', '', '', true);
    wp_enqueue_script('jquery.isotope.min.js', get_stylesheet_directory_uri() . '/assets/vendor/isotope/jquery.isotope.min.js', '', '', true);
    wp_enqueue_script('owl.carousel.min.js', get_stylesheet_directory_uri() . '/assets/vendor/owl.carousel/owl.carousel.min.js', '', '', true);
    wp_enqueue_script('jquery.magnific-popup.min.js', get_stylesheet_directory_uri() . '/assets/vendor/magnific-popup/jquery.magnific-popup.min.js', '', '', true);
    wp_enqueue_script('jquery.vide.min.js', get_stylesheet_directory_uri() . '/assets/vendor/vide/jquery.vide.min.js', '', '', true);
    wp_enqueue_script('vivus.min.js', get_stylesheet_directory_uri() . '/assets/vendor/vivus/vivus.min.js', '', '', true);
    wp_enqueue_script('theme.js', get_stylesheet_directory_uri() . '/assets/js/theme.js', '', '', true);
    wp_enqueue_script('jquery.themepunch.tools.min.js', get_stylesheet_directory_uri() . '/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js', '', '', true);
    wp_enqueue_script('jquery.themepunch.revolution.min.js', get_stylesheet_directory_uri() . '/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js', '', '', true);
    wp_enqueue_script('jquery.flipshow.min.js', get_stylesheet_directory_uri() . '/assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js', '', '', true);
    wp_enqueue_script('view.home.js', get_stylesheet_directory_uri() . '/assets/js/views/view.home.js', '', '', true);
    wp_enqueue_script('custom.js', get_stylesheet_directory_uri() . '/assets/js/custom.js', '', '', true);
    wp_enqueue_script('theme.init.js', get_stylesheet_directory_uri() . '/assets/js/theme.init.js', '', '', true);

    //Javascript Opera2
    wp_enqueue_script('script.o2', get_stylesheet_directory_uri() . '/script.js', '', '', true);
}

add_action('wp_enqueue_scripts', 'wpdocs_scripts_method');


//Deixar somente número
function deixarnumeros($str) {
    return preg_replace("/[^0-9]/", "", $str);
}
