<?php get_header(); ?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php post_type_archive_title(); ?></h1>
                    <span class="sub-title text-dark">Selecione qual tipo de ajuda você precisa e veja pessoas que estão próximas a você e que podem ajudá-lo.</span>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php post_type_archive_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <?php if (have_posts()) { ?>
                        <div class="row">
                            <?php
                            while (have_posts()) {
                                the_post();
                                $foto = get_field('imagem');
                                if (is_user_logged_in()) {
                                    ?>
                                    <div class="col-md-4 col-lg-4 mb-4">
                                        <div class="card">
                                            <?php
                                            if ($foto) {
                                                ?>
                                                <a href="<?php the_permalink() ?>"><img src="<?php echo $foto['url'] ?>" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="<?php the_title() ?>"></a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ajuda.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="<?php the_title() ?>"></a>
                                                <?php
                                            }
                                            ?>
                                            <div class="card-body">
                                                <h4 class="card-title mb-1 text-4 font-weight-bold"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                                                <p class="card-text"><?php echo wp_strip_all_tags(string_limit_words(get_field('detalhes_da_ajuda'), 22)); ?></p>
                                                <a href="<?php the_permalink() ?>" class="read-more text-color-primary font-weight-semibold text-2">Ver mais <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-md-4 col-lg-3">
                                        <div class="card">
                                            <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar.jpg" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="<?php the_title() ?>"></a>
                                            <h4 class="mt-2 mb-2 text-center"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-warning margin-100px-top" role="alert"><strong>Voluntários não encontrados!</strong><br>Não encontramos nenhum voluntário cadastrados para sua região, se você quer ser um voluntário, <a href="<?php echo get_permalink(get_page_by_path('criar-conta')) ?>">clique aqui</a> e crie seu perfil.</div>
                        <?php } ?>
                        <?php include 'pagination.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>