<?php

function wpdocs_scripts_method_page() {
//Javascript Template
    wp_enqueue_script('maps.googleapis.js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC2034wrxhRFDr9IMKbGcD_dPIbY-fdzCM', '', '', true);
    wp_enqueue_script('maps.js', get_stylesheet_directory_uri() . '/assets/js/maps.js', '', '', true);
}

add_action('wp_enqueue_scripts', 'wpdocs_scripts_method_page');
get_header();
?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4 mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <?php if (is_user_logged_in()) { ?>
                        <div class="row">
                            <div class="col-lg-3 mt-4 mt-lg-0">

                                <div class="d-flex justify-content-center mb-4">
                                    <div class="profile-image-outer-container">
                                        <div class="profile-image-inner-container bg-color-primary">
                                            <?php
                                            $foto = get_field('foto', 'user_' . $post->post_author);
                                            if ($foto) {
                                                ?>
                                                <img src="<?php echo $foto ?>">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar.jpg">
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <aside class="sidebar mt-2" id="sidebar">
                                    <ul class="nav nav-list flex-column mb-5">
                                        <li class="nav-item"><a class="nav-link" href="#contato">Contato</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#redessociais">Redes Sociais</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#atuacao">Atuação</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#ajuda">Ajuda</a></li>
                                    </ul>
                                </aside>

                            </div>
                            <div class="col-lg-9">
                                <?php
                                $permissoes = get_field('permissoes');
                                if ($post->post_author == get_current_user_id() && $permissoes) {
                                    ?>
                                    <h4>Usuários que possuem acesso ao seu perfil</h4>
                                    <div class="owl-carousel owl-theme stage-margin nav-style-1 " data-plugin-options="{'items': 6, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
                                        <?php
                                        foreach ($permissoes as $permissoe) {
                                            ?>
                                            <div>
                                                <?php
                                                $foto = get_field('foto', 'user_' . $permissoe);
                                                if ($foto) {
                                                    ?>
                                                <a href="<?php the_permalink(get_field('voluntario')) ?>" target="_blank"><img src="<?php echo $foto ?>"></a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="<?php the_permalink(get_field('voluntario')) ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar.jpg"></a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                if (in_array(get_current_user_id(), get_field('permissoes', 'user_' . $post->post_author)) || $post->post_author == get_current_user_id()) {
                                    ?>
                                    <hr>
                                    <h3 id="contato">Contato</h3>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">E-mail <span class="color-red">*</span></label>
                                        <div class="col-lg-9">
                                            <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $current_user->user_email ?>" target="_blank"><?php echo $current_user->user_email ?></a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">WhatsApp</label>
                                        <div class="col-lg-9">
                                            <i class="fab fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=55<?php echo deixarnumeros(get_field('whatsapp', 'user_' . $post->post_author)) ?>" target="_blank"><?php the_field('whatsapp', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Skype</label>
                                        <div class="col-lg-9">
                                            <i class="fab fa-skype"></i> <a href="skype:<?php the_field('skype', 'user_' . $post->post_author) ?>" target="_blank"><?php the_field('skype', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Telefone</label>
                                        <div class="col-lg-9">
                                            <i class="fa fa-phone"></i> <a href="tel:<?php echo deixarnumeros(get_field('telefone', 'user_' . $post->post_author)) ?>" target="_blank"><?php the_field('telefone', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>

                                    <hr>
                                    <h3 id="redessociais">Redes Sociais</h3>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Facebook</label>
                                        <div class="col-lg-9">
                                            <i class="fab fa-facebook"></i> <a href="<?php the_field('facebook', 'user_' . $post->post_author) ?>" target="_blank"><?php the_field('facebook', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Instagram</label>
                                        <div class="col-lg-9">
                                            <i class="fab fa-instagram"></i> <a href="<?php the_field('instagram', 'user_' . $post->post_author) ?>" target="_blank"><?php the_field('instagram', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">LinkedIn</label>
                                        <div class="col-lg-9">
                                            <i class="fab fa-linkedin"></i> <a href="<?php the_field('linkedin', 'user_' . $post->post_author) ?>" target="_blank"><?php the_field('linkedin', 'user_' . $post->post_author) ?></a>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-warning">
                                        <h2 class="mb-2">Solicitar acesso</h2>
                                        Para garantir a segurança dessa pessoa, os dados de contato só serão liberados após ela aceitar. Para solicitar os dados de contato, clique aqui ou converse com ela através de nosso sistema de comentário.
                                    </div>
                                <?php } ?>

                                <hr>
                                <h3 id="redessociais">Local</h3>
                                <div class="form-group row">
                                    <label class="col-lg-3 font-weight-bold text-dark col-form-label form-control-label text-2">Cidade</label>
                                    <div class="col-lg-9">
                                        <i class="fa fa-map-marker-alt"></i> <?php the_field('cidade', 'user_' . $post->post_author) ?> - <?php the_field('estado', 'user_' . $post->post_author) ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php
                        $ajudas = get_field('ajuda', 'user_' . $post->post_author);
                        if ($ajudas) {
                            ?>
                            <hr>
                            <h3 id="ajuda">Ajudas Oferecidas</h3>
                            <div class="row">
                                <?php
                                foreach ($ajudas as $ajuda) {
                                    ?>
                                    <div class="col-md-6 col-lg-6 mb-3">
                                        <div class="card bg-color-grey">
                                            <div class="card-body">
                                                <h4 class="m-0">
                                                    <?php echo get_the_title($ajuda); ?>
                                                </h4>
                                                <?php the_field('detalhes_da_ajuda', $ajuda) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>
                            </div>
                        <?php } ?>

                        <hr>
                        <h3 id="atuacao">Atuação</h3>
                        <div class="google-map-borders">
                            <div id="googlemaps" data-latitude="<?php the_field('latitude', 'user_' . $post->post_author) ?>" data-longitude="<?php the_field('longitude', 'user_' . $post->post_author) ?>"data-atuacao="<?php the_field('atuacao', 'user_' . $post->post_author) ?>" class="google-map mt-0 mb-0" style="height: 370px;"></div>
                        </div>
                        <?php include_once 'comments.php'; ?>
                    <?php } else { ?>
                        <div class="alert alert-warning">
                            <h2 class="mb-2">Acesso restrito</h2>
                            Para ver as informações desse voluntário é preciso criar uma conta no sistema.</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>