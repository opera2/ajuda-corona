<?php get_header(); ?>
<div role="main" class="main">
    <div class="slider-container rev_slider_wrapper" style="height: 100vh;">
        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1170, 'gridheight': 1000, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500], 'parallax': { 'type': 'scroll', 'origo': 'enterpoint', 'speed': 1000, 'levels': [2,3,4,5,6,7,8,9,12,50], 'disable_onmobile': 'on' }}">
            <ul>
                <li class="slide-overlay" data-transition="fade">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blank.gif"  
                         alt=""
                         data-bgposition="center center" 
                         data-bgfit="cover" 
                         data-bgrepeat="no-repeat" 
                         class="rev-slidebg">

                    <div class="rs-background-video-layer" 
                         data-forcerewind="on" 
                         data-volume="mute" 
                         data-videowidth="100%" 
                         data-videoheight="100%" 
                         data-videomp4="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" 
                         data-videopreload="preload" 
                         data-videoloop="loop" 
                         data-forceCover="1" 
                         data-aspectratio="16:9" 
                         data-autoplay="true" 
                         data-autoplayonlyfirsttime="false" 
                         data-nextslideatend="false">
                    </div>

                    <div class="tp-caption"
                         data-x="center" data-hoffset="['-125','-125','-125','-215']"
                         data-y="center" data-voffset="['-50','-50','-50','-75']"
                         data-start="1000"
                         data-transform_in="x:[-300%];opacity:0;s:500;"
                         data-transform_idle="opacity:0.5;s:500;" style="z-index: 5;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/slides/slide-title-border-light.png" alt=""></div>

                    <div class="tp-caption text-color-dark font-weight-normal"
                         data-x="center"
                         data-y="center" data-voffset="['-50','-50','-50','-75']"
                         data-start="700"
                         data-fontsize="['22','22','22','40']"
                         data-lineheight="['25','25','25','45']"
                         data-transform_in="y:[-50%];opacity:0;s:500;" style="z-index: 5;">TODOS CONTRA O</div>

                    <div class="tp-caption d-none d-md-block"
                         data-frames='[{"delay":2400,"speed":500,"frame":"0","from":"opacity:0;x:10%;","to":"opacity:1;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-x="center" data-hoffset="['72','72','72','127']"
                         data-y="center" data-voffset="['-33','-33','-33','-55']" style="z-index: 5;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/slides/slide-blue-line.png" alt=""></div>

                    <div class="tp-caption"
                         data-x="center" data-hoffset="['125','125','125','215']"
                         data-y="center" data-voffset="['-50','-50','-50','-75']"
                         data-start="1000"
                         data-transform_in="x:[300%];opacity:0;s:500;"
                         data-transform_idle="opacity:0.5;s:500;" style="z-index: 5;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/slides/slide-title-border-light.png" alt=""></div>

                    <div class="tp-caption font-weight-extra-bold text-color-dark negative-ls-2"
                         data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                         data-x="center"
                         data-y="center"
                         data-fontsize="['50','50','50','90']"
                         data-lineheight="['55','55','55','95']" style="z-index: 5;">CORONA VIRUS</div>

                    <div class="tp-caption font-weight-light text-color-dark"
                         data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-x="center"
                         data-y="center" data-voffset="['40','40','40','80']"
                         data-fontsize="['18','18','18','50']"
                         data-lineheight="['20','20','20','55']"
                         style="z-index: 5;">Seja um voluntário e ajude sua comunidade</div>

                    <a class="tp-caption slider-scroll-button slider-scroll-button-dark"
                       data-hash
                       data-hash-offset="80"
                       href="#main"
                       data-x="center"
                       data-y="bottom" data-voffset="['30','30','30','30']"
                       data-start="1600"					 
                       data-transform_in="y:[100%];s:500;"
                       data-transform_out="y:[100%];opacity:0;s:500;"
                       data-mask_in="x:0px;y:0px;" style="z-index: 5;"></a>

                    <div class="tp-dottedoverlay tp-opacity-overlay-light opacity-8"></div>
                </li>
            </ul>
        </div>

    </div>

    <?php
    $sintomas = new WP_Query(array('post_type' => 'sintomas', 'posts_per_page' => '-1', 'orderby' => 'rand'));
    if ($sintomas->have_posts()) {
        ?>
        <section class="section border-0 m-0">
            <div class="container">
                <div class="divider"><i class="fas fa-disease"></i></div>
                <h4>Sintomas</h4>
                <div class="row mb-5 pb-3">
                    <?php
                    $i = 1;
                    while ($sintomas->have_posts()) {
                        $sintomas->the_post();
                        $foto = get_field('foto');
                        ?>

                        <div class="col-md-6 col-lg-4 mb-5 mt-5 mb-lg-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
                            <div class="card flip-card text-center rounded-0">
                                <div class="flip-front p-5">
                                    <div class="flip-content my-4">
                                        <strong class="font-weight-extra-bold text-color-dark line-height-1 text-13 mb-3 d-inline-block"><?php echo $i++ ?></strong>
                                        <h4 class="font-weight-bold text-color-primary text-4"><?php the_title(); ?></h4>
                                    </div>
                                </div>
                                <div class="flip-back d-flex align-items-center p-5" style="background-image: url(<?php echo $foto['url'] ?>); background-size: cover; background-position: center;">
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>
        <?php
    }
    ?>

    <?php
    $midias = new WP_Query(array('post_type' => 'midias', 'posts_per_page' => '-1', 'orderby' => 'rand'));
    if ($midias->have_posts()) {
        ?>
        <div class="owl-carousel owl-theme full-width mb-0" data-plugin-options="{'items': 5, 'loop': false, 'nav': true, 'dots': false}">
            <?php
            while ($midias->have_posts()) {
                $midias->the_post();
                $imagem = get_field('imagem');
                ?>
                <div>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                            <span class="thumb-info-wrapper">
                                <img src="<?php echo $imagem['sizes']['thumbnail']; ?>" class="img-fluid" alt="">
                                <span class="thumb-info-title">
                                    <span class="thumb-info-inner"><?php the_title(); ?></span>
                                </span>
                            </span>
                        </span>
                    </a>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
        </div>
    <?php } ?>

    <?php
    $voluntarios = new WP_Query(array('post_type' => 'voluntarios', 'posts_per_page' => '25', 'orderby' => 'rand'));
    if ($voluntarios->have_posts()) {
        ?>
        <section class="section border-0 m-0">
            <div class="container">
                <div class="divider"><i class="fas fa-life-ring"></i></div>
                <h4>Voluntários</h4>
                <div class="owl-carousel owl-theme dots-morphing" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 3}}, 'loop': false, 'autoHeight': true, 'margin': 10}">
                    <?php
                    while ($voluntarios->have_posts()) {
                        $voluntarios->the_post();
                        $foto = get_field('foto', 'user_' . $post->post_author);
                        if ($foto) {
                            $perfil = $foto;
                        } else {
                            $perfil = get_template_directory_uri() . '/assets/img/avatar.jpg';
                        }
                        ?>
                        <div>
                            <div class="card card-background-image-hover border-0" style="background-image: url(<?php echo $perfil; ?>);">
                                <div class="card-body text-center p-5">
                                    <i class="fa fa-life-ring text-color-primary text-10"></i>
                                    <h4 class="card-title mt-2 mb-2 text-5 font-weight-bold"><?php the_title(); ?></h4>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-dark btn-modern">Ver perfil</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>
    <?php } ?>
</div>

<section class="section bg-color-grey-scale-1 border-0 m-0">
    <div class="container">
        <div class="row my-5">
            <div class="col-lg-6 pr-5">
                <h2 class="font-weight-normal text-6 mb-2 pb-1">Grupos de <strong class="font-weight-extra-bold">risco</strong></h2>
                <p class="lead">Quais grupos são mais vulneráveis ao coronavírus?</p>
                <p>Médicos explicam que idosos, diabéticos, hipertensos e quem tem insuficiência cardíaca, renal ou doença respiratória crônica podem ficar mais expostos e ter complicações decorrentes da Covid-19</p>
            </div>
            <div class="col-lg-6">
                <div class="progress-bars mt-5">
                    <div class="progress-label">
                        <span>Idosos</span>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
                            <span class="progress-bar-tooltip">100%</span>
                        </div>
                    </div>
                    <div class="progress-label">
                        <span>Hipertensos</span>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
                            <span class="progress-bar-tooltip">100%</span>
                        </div>
                    </div>
                    <div class="progress-label">
                        <span>Asmáticos</span>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
                            <span class="progress-bar-tooltip">100%</span>
                        </div>
                    </div>
                    <div class="progress-label">
                        <span>Diabéticos</span>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
                            <span class="progress-bar-tooltip">100%</span>
                        </div>
                    </div>
                    <div class="progress-label">
                        <span>Fumantes</span>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
                            <span class="progress-bar-tooltip">100%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--    <section class="section section-height-3 border-0 m-0 appear-animation" data-appear-animation="fadeIn">
        <div class="container">
            <div class="row">
                <div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">

                    <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
                        <div>
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                                <div class="testimonial-author">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clients/client-1.jpg" class="img-fluid rounded-circle" alt="">
                                </div>
                                <blockquote>
                                    <p class="text-color-dark text-5 line-height-5 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ante tellus, convallis non consectetur sed, pharetra nec ex.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong class="font-weight-extra-bold text-2">- John Smith. Okler</strong></p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                                <div class="testimonial-author">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clients/client-1.jpg" class="img-fluid rounded-circle" alt="">
                                </div>
                                <blockquote>
                                    <p class="text-color-dark text-5 line-height-5 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong class="font-weight-extra-bold text-2">- John Smith. Okler</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>-->

</div>
<?php get_footer(); ?>