//$("#googlemaps").gMap({
//    controls: {
//        draggable: (($.browser.mobile) ? false : true),
//        panControl: true,
//        zoomControl: true,
//        mapTypeControl: true,
//        scaleControl: true,
//        streetViewControl: true,
//        overviewMapControl: true
//    },
//    scrollwheel: false,
//    markers: [{
//            latitude: $('#googlemaps').data('latitude'),
//            longitude: $('#googlemaps').data('longitude'),
//            html: "<strong>" + $('#googlemaps').data('title') + "</strong><br>" + $('#googlemaps').data('address'),
//            icon: {
//                image: $("#siteurl").data('url') + "/assets/img/pin.png",
//                iconsize: [26, 46],
//                iconanchor: [12, 46]
//            }
//        }],
//    latitude: $('#googlemaps').data('latitude'),
//    longitude: $('#googlemaps').data('longitude'),
//    zoom: 14,
//    strokeColor: "#c3fc49",
//    strokeOpacity: 0.8,
//    strokeWeight: 2,
//    fillColor: "#c3fc49",
//    fillOpacity: 0.35,
//    map: $("#googlemaps"),
//    center: {lat: 41.878, lng: -87.629},
//    radius: 1500
//});


var map;
var latlng = new google.maps.LatLng($('#googlemaps').data('latitude'), $('#googlemaps').data('longitude'));
function initialize() {
    var mapOptions = {
        center: latlng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var el=document.getElementById("googlemaps");
    map = new google.maps.Map(el, mapOptions);

    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
    });

    var sunCircle = {
        strokeColor: "#005045",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#80B666",
        fillOpacity: 0.35,
        map: map,
        center: latlng,
        radius: $('#googlemaps').data('atuacao') * 1000 // in meters
    };
    cityCircle = new google.maps.Circle(sunCircle);
    cityCircle.bindTo('center', marker, 'position');
}
initialize();