<?php

//---------------------------------Ajudas
add_action("init", "codex_ajudas_init");

function codex_ajudas_init() {
    $ao = "a";
    $singular = "Ajuda";
    $plural = "Ajudas";
    $slug = "ajudas";
    $icon = "dashicons-buddicons-replies";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title", "author")
    );
    register_post_type($slug, $args);
}

//---------------------------------Voluntários
add_action("init", "codex_voluntarios_init");

function codex_voluntarios_init() {
    $ao = "o";
    $singular = "Voluntário";
    $plural = "Voluntários";
    $slug = "voluntarios";
    $icon = "dashicons-buddicons-buddypress-logo";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title", "author", "comments")
    );
    register_post_type($slug, $args);
}

//---------------------------------Cuidados
add_action("init", "codex_cuidados_init");

function codex_cuidados_init() {
    $ao = "o";
    $singular = "Cuidado";
    $plural = "Cuidados";
    $slug = "cuidados";
    $icon = "dashicons-book";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title")
    );
    register_post_type($slug, $args);
}

//---------------------------------Empresas
add_action("init", "codex_empresas_init");

function codex_empresas_init() {
    $ao = "a";
    $singular = "Empresa";
    $plural = "Empresas";
    $slug = "empresas";
    $icon = "dashicons-store";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title")
    );
    register_post_type($slug, $args);
}

//---------------------------------Sintomas
add_action("init", "codex_sintomas_init");

function codex_sintomas_init() {
    $ao = "o";
    $singular = "Sintoma";
    $plural = "Sintomas";
    $slug = "sintomas";
    $icon = "dashicons-store";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title")
    );
    register_post_type($slug, $args);
}

//---------------------------------Midias
add_action("init", "codex_midias_init");

function codex_midias_init() {
    $ao = "a";
    $singular = "Mídia";
    $plural = "Mídias";
    $slug = "midias";
    $icon = "dashicons-store";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title")
    );
    register_post_type($slug, $args);
}

//---------------------------------Necessidades
add_action("init", "codex_necessidades_init");

function codex_necessidades_init() {
    $ao = "a";
    $singular = "Necessidade";
    $plural = "Necessidades";
    $slug = "necessidades";
    $icon = "dashicons-store";
    $labels = array(
        "name" => _x($plural, 'post type general name'),
        "singular_name" => _x($singular, 'post type singular name'),
        "menu_name" => _x($plural, 'admin menu'),
        "name_admin_bar" => _x($singular, 'add new on admin bar'),
        "add_new" => _x("Nov$ao $singular", $slug),
        "add_new_item" => __("Adicionar Nov$ao $singular"),
        "new_item" => __("Nov$ao $singular"),
        "edit_item" => __("Editar $singular"),
        "view_item" => __("Visualizar $singular"),
        "all_items" => __("Tod" . $ao . "s " . $ao . "s $plural"),
        "search_items" => __("Pesquisar $plural"),
        "parent_item_colon" => __("$singular Pai:"),
        "not_found" => __("$singular não encontrad$ao."),
        "not_found_in_trash" => __("$singular não encontrad$ao na Lixeira.")
    );
    $args = array(
        "menu_icon" => $icon,
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "query_var" => true,
        "rewrite" => array("slug" => $slug),
        "capability_type" => "post",
        "has_archive" => true,
        "hierarchical" => false,
        "menu_position" => null,
        "supports" => array("title", "author")
    );
    register_post_type($slug, $args);
}

//Categorias------------------------------

add_action("init", "categorias");

function categorias() {
    register_taxonomy('cidades', array('empresas'), array(
        'hierarchical' => true,
        'label' => __('Cidades'),
        'show_ui' => true,
        'show_in_tag_cloud' => true,
        'query_var' => true,
        'rewrite' => true,
    ));
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
    echo '<style>#cidadesdiv, #tiposdiv{display: none;}</style>';
}