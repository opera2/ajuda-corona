<?php get_header(); ?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php post_type_archive_title(); ?></h1>
                    <span class="sub-title text-dark">Conheça quem faz parte dessa conrrente do bem e faça parte você também.</span>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php post_type_archive_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <a href="<?php echo get_permalink(get_page_by_path('necessidades-criar')) ?>" class="btn btn-success">Cadastrar necessidade</a>
                    <hr>
                    <?php if (have_posts()) { ?>
                        <div class="row">
                            <?php
                            while (have_posts()) {
                                the_post();
                                $foto = get_field('foto', 'user_' . $post->post_author);
                                ?>
                                <div class="col-md-6 col-lg-6 mb-6 mb-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php the_title() ?></h4>
                                            <p><?php echo wp_strip_all_tags(string_limit_words(get_field('descricao'), 22)); ?></p>
                                            <a href="<?php the_permalink() ?>" class="btn btn-success">Detelahes</a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-warning margin-100px-top" role="alert"><strong>Necessidades não encontrados!</strong><br>Não encontramos nenhuma necessidade cadastrados para sua região, se você quer ser um voluntário, <a href="<?php echo get_permalink(get_page_by_path('criar-conta')) ?>">clique aqui</a> e crie seu perfil.</div>
                        <?php } ?>
                        <?php include 'pagination.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>