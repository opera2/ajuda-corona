<?php get_header(); ?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">
        <?php
        $json = file_get_contents('https://www.gaialongevidade.com.br/rss-2/');
        $obj = json_decode($json);
        ?>
        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <h4>Terceira idade</h4>
                    <div class="owl-carousel owl-theme dots-morphing" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 3}}, 'loop': false, 'autoHeight': true, 'margin': 10}">
                        <?php
                        foreach ($obj as $item) {
                            ?>
                            <div>
                                <div class="card flip-card text-center rounded-0">
                                    <div class="flip-front p-5">
                                        <div class="flip-content my-4">
                                            <h4 class="font-weight-bold text-color-primary text-4"><?php echo $item->title; ?></h4>
                                            <p>
                                                <?php echo wp_strip_all_tags(string_limit_words($item->content, 22)); ?><br>
                                                <i class="fa fa-calendar-alt"></i> <?php echo $item->date; ?>
                                            </p>
                                            <a href="<?php echo $item->link; ?>" class="btn btn-light btn-modern text-color-dark font-weight-bold" target="_blank">Leia mais</a>
                                        </div>
                                    </div>
                                    <div class="flip-back d-flex align-items-center p-5 overlay overlay-color-dark" style="background-image: url(<?php echo $item->image; ?>); background-size: cover; background-position: center;">
                                        <div class="flip-content my-4">
                                            <h4 class="font-weight-bold text-color-light"><?php echo $item->title; ?></h4>
                                            <p class="font-weight-light text-color-light opacity-5">
                                                <?php echo wp_strip_all_tags(string_limit_words($item->content, 22)); ?><br>
                                                <i class="fa fa-calendar-alt"></i> <?php echo $item->date; ?>
                                            </p>
                                            <a href="<?php echo $item->link; ?>" class="btn btn-light btn-modern text-color-dark font-weight-bold" target="_blank">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>




        <div class="divider divider-solid divider-style-4 taller"><i class="fas fa-chevron-down"></i></div>
        <?php
        $rss_feed = simplexml_load_file("https://g1.globo.com/rss/g1/ciencia-e-saude/");
        ?>
        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <h4>Notícias G1</h4>
                    <div class="owl-carousel owl-theme dots-morphing" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 3}}, 'loop': false, 'autoHeight': true, 'margin': 10}">
                        <?php
                        foreach ($rss_feed->channel->item as $feed_item) {
                            $pos = strpos($feed_item->description, 'coronavírus');
                            ?>
                            <div>
                                <div class="card text-center rounded-0">
                                    <div class="flip-front p-5">
                                        <div class="flip-content my-4">
                                            <h4 class="font-weight-bold text-color-primary text-4"><?php echo $feed_item->title; ?></h4>
                                            <p>
                                                <?php echo wp_strip_all_tags(string_limit_words($feed_item->description, 22)); ?><br>
                                                <i class="fa fa-calendar-alt"></i> <?php echo date("d/m/Y", strtotime($feed_item->pubDate)); ?>
                                            </p>
                                            <a href="<?php echo $feed_item->link; ?>" class="btn btn-light btn-modern text-color-dark font-weight-bold" target="_blank">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="divider divider-solid divider-style-4 taller"><i class="fas fa-chevron-down"></i></div>
        <?php
        $rss_feed = simplexml_load_file("https://feeds.folha.uol.com.br/ciencia/rss091.xml");
        ?>
        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <h4>Notícias UOL</h4>
                    <div class="owl-carousel owl-theme dots-morphing" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 3}}, 'loop': false, 'autoHeight': true, 'margin': 10}">
                        <?php
                        foreach ($rss_feed->channel->item as $feed_item) {
                            $pos = strpos($feed_item->description, 'coronavírus');
                            ?>
                            <div>
                                <div class="card text-center rounded-0">
                                    <div class="flip-front p-5">
                                        <div class="flip-content my-4">
                                            <h4 class="font-weight-bold text-color-primary text-4"><?php echo $feed_item->title; ?></h4>
                                            <p>
                                                <?php echo wp_strip_all_tags(string_limit_words($feed_item->description, 22)); ?><br>
                                                <i class="fa fa-calendar-alt"></i> <?php echo date("d/m/Y", strtotime($feed_item->pubDate)); ?>
                                            </p>
                                            <a href="<?php echo $feed_item->link; ?>" class="btn btn-light btn-modern text-color-dark font-weight-bold" target="_blank">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>