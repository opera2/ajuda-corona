<?php if (is_user_logged_in()) { ?>
    <hr>
    <div id="comments" class="post-block mt-5 post-comments">
        <h4 class="mb-3">Mensagens</h4>
        <ul class="comments">
    <!--<pre>-->
            <?php
            $action = filter_input(INPUT_POST, 'action');
            if (!empty($action)) {
                $current_user = wp_get_current_user();
                $comment_post_ID = filter_input(INPUT_POST, 'comment_post_ID', FILTER_SANITIZE_STRING);
                $comment = $_POST['comment'];

                $data = array(
                    'comment_post_ID' => $comment_post_ID,
                    'comment_content' => $comment,
                    'comment_parent' => 0,
                    'user_id' => get_current_user_id(),
                    'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
                    'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
                    'comment_approved' => 1,
                );

                wp_insert_comment($data);

                if ($current_user->roles[0] == 'subscriber' || $current_user->roles[0] == 'contributor') {
                    update_field('read_validator', 1);
                } elseif (get_field('validador', 'user_' . $current_user->ID)) {
                    update_field('read_user', 1);

                    $bodycore = '<table class="table table-bordered table-striped"><tr><td><center><img src="' . get_template_directory_uri() . '/assets/images/logo@.png" alt="' . get_bloginfo('name') . '"></center></td></tr>'
                            . '<tr><td>'
                            . '<p>Um novo comentário foi feito no perfil cadastrado no sistema. Clique no link abaixo para acessar o perfil.</p>'
                            . '<tr><td><a href="' . get_the_permalink($comment_post_ID) . '">' . get_the_permalink($comment_post_ID) . '</a></td></tr>'
                            . '</td></tr>'
                            . '</table><br>';

                    $body = html_template($bodycore);

                    $email = sendEmail(array('AddAddress' => get_the_author_meta('user_email', $post->post_author), 'name' => get_the_author_meta('first_name', $post->post_author), 'subject' => 'Alteração de informações - ' . get_bloginfo('name'), 'body' => $body));
//                $email = sendEmail(array('AddAddress' => 'eduardo.nascimento@opera2.com.br', 'name' => get_the_author_meta('first_name', $post->post_author), 'subject' => 'Novo comentário - ' . get_bloginfo('name'), 'body' => $body));
                }
            }


            $comments_query = new WP_Comment_Query;
            $comments = $comments_query->query(array('post_id' => get_the_ID()));

            if ($comments) {
                foreach ($comments as $comment) {
                    $user_info = get_userdata($comment->user_id);
                    ?>
                    <li>
                        <div class="comment">
                            <div class="img-thumbnail img-thumbnail-no-borders d-none d-sm-block">
                                <?php
                                $foto = get_field('foto', 'user_' . $user_info->ID);
                                if ($foto) {
                                    ?>
                                    <img src="<?php echo $foto ?>" class="avatar">
                                    <?php
                                } else {
                                    ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar.jpg" class="avatar">
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="comment-block">
                                <div class="comment-arrow"></div>
                                <span class="comment-by">
                                    <strong><?php echo $user_info->first_name ?></strong>
                                </span>
                                <p><?php echo nl2br($comment->comment_content); ?></p>
                                <span class="date float-right"><?php echo date("d/m/Y h:i", strtotime($comment->comment_date)); ?></span>
                            </div>
                        </div>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <?php
    } else {
        echo '<div class="alert alert-warning">Nenhuma mensagem foi enviada ainda.</div>';
    }
    ?>
    <!--</pre>-->

    <h3 id="atuacao">Deixar uma mensagem</h3>
    <form method="POST">
        <p class="comment-form-comment">
            <label for="comment">Envie uma mensagem</label>
            <textarea class="form-control" name="comment" cols="45" rows="4" aria-required="true"></textarea>
        </p>

        <p class="form-submit">
            <button class="btn btn-success">Enviar</button>
            <input type="hidden" name="comment_post_ID" value="<?php the_ID() ?>">
            <input type="hidden" name="action" value="comentarios">
            <input type="hidden" name="comment_parent" value="0">
        </p>
    </form>
<?php }else{ ?>
    <div class="alert alert-warning">Para deixar seu comentário, crie uma conta ou faça o login.</div>
<?php } ?>
