<footer id="footer" class="mt-0">
    <div class="container">
        <div class="footer-ribbon">
            <span><?php bloginfo() ?></span>
        </div>
        <div class="row py-5 my-4">
            <div class="col-md-6 mb-4 mb-lg-0">
                <a href="index.html" class="logo pr-0 pr-lg-3">
                    <img alt="Porto Website Template" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-footer.png" class="opacity-7 bottom-4" height="33">
                </a>
                <p class="mt-2 mb-2">Esse projeto tem como objetivo ajudar pessoas que estão no grupo de risco do coronavírus e precisam de ajuda para executar algumas tarefas. Se você pode ajudar, cadastre-se no site e seja voluntário.</p>
                <p class="mt-2 mb-2">Consulte nossos <a href="<?php echo get_permalink(get_page_by_path('termos-de-uso')) ?>">termos de uso</a>.</p>
            </div>
            <div class="col-md-6">
                <h5 class="text-3 mb-3">Apoaiadores</h5>
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list list-icons list-icons-lg">
                            <li><i class="fas fa-angle-right"></i><a href="https://www.gaialongevidade.com.br" class="link-hover-style-1 ml-1"> Gaia Longevidade</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list list-icons list-icons-lg">
                            <li><i class="fas fa-angle-right"></i><a href="https://www.opera2.com.br" class="link-hover-style-1 ml-1"> Opera2</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright footer-copyright-style-2">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col d-flex align-items-center justify-content-center">
                    <p>Desenvolvido por <a href="http://www.opera2.com.br" target="_blank">Opera2</a>.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<div class="modal fade" tabindex="-1" id="modalAjaxForm" role="dialog" aria-labelledby="modalAjaxForm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="modalAjaxFormContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>