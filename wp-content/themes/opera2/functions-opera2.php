<?php

//Custom WordPress Login Logo by WpTotal.com.br
function cutom_login_logo() {
    echo "<style type=\"text/css\">
body.login div#login h1 a {
background-image: url(" . get_bloginfo('template_directory') . "/img-opera2/login-opera2.png);
-webkit-background-size: auto;
background-size: auto;
margin: 0 0 25px;
width: 320px;
}
#backtoblog{
display:none;
}
#nav{
text-align:center;
}
</style>";
}

add_action('login_enqueue_scripts', 'cutom_login_logo');

// THIS ADDS CUSTOM LOGO TO ADMIN DASHBOARD
add_action('admin_head', 'my_custom_logo');

function my_custom_logo() {
    echo '<style type="text/css">
    #header-logo { background-image: url(' . get_bloginfo('template_directory') . '/img-opera2/top-opera2.png) !important; }
    </style>';
}

function remove_footer_admin() {
    echo '© <a href="http://www.opera2.com.br/">Opera2</a> - Todos os direitos reservados.';
}

add_filter('admin_footer_text', 'remove_footer_admin');

function alterar_admin_bar($admin_bar) {

    // Remove o logotipo
    $admin_bar->remove_menu('wp-logo');

    // Remove o menu suspenso de adição de novo conteúdo
    $admin_bar->remove_node('new-content');

    // Remove o link para editar a página atual
    $admin_bar->remove_menu('edit');

    // Remove o notificador de atualizações
    $admin_bar->remove_menu('updates');

    // Remove o menu de pesquisa
    $admin_bar->remove_menu('search');

    // Remove o balão de comentários
    $admin_bar->remove_menu('comments');

    // Remove o menu suspenso com o nome do site
    $admin_bar->remove_node('site-name');

    // Remove o menu suspenso da conta do usuário
    $admin_bar->remove_node('my-account');

    /**
     * Vamos adicionar novos elementos à barra
     * 
     */
    // Adicionar a descrição do website
    $admin_bar->add_node(array(
        'id' => 'description',
        'title' => '<img src="' . get_bloginfo('template_directory') . '/img-opera2/top-opera2.png" style="height: 20px; padding: 4px 10px 0 0; float: left;" /> ' . get_bloginfo('name'),
        'href' => site_url(),
        'meta' => array(
            'class' => 'description',
        ),
            )
    );

    // Link da homepage para ser usado nas funções
    // wp_login_url e wp_logout_url
    $redirect = site_url();


    // Verifica se o usuário está logado
    if (is_user_logged_in()) :

        // Este é um pequeno hack necessário para
        // podermos usar o nome do nome do usuário
        global $current_user;

        // Vamos adicionar uma mensagem de boas-vindas e 
        // um link para o usuário poder fazer logout
        $admin_bar->add_menu(array(
            'id' => 'welcome',
            'parent' => 'top-secondary',
            'title' => sprintf('Bem-vindo %s', $current_user->display_name),
            'href' => admin_url('profile.php'),
            'meta' => array(
                'class' => 'link-profile',
            ),
                )
        );

        $admin_bar->add_menu(array(
            'id' => 'logout',
            'parent' => 'top-secondary',
            'title' => 'Logout',
            'href' => wp_logout_url($redirect),
            'meta' => array(
                'class' => 'link-logout',
            ),
                )
        );


    else :

        // O usuário não está logado
        // Adicionar links para login e registo

        $admin_bar->add_menu(array(
            'id' => 'register',
            'parent' => 'top-secondary',
            'title' => 'Não tem login? Registe-se!',
            'href' => wp_login_url($redirect) . '?action=register',
            'meta' => array(
                'class' => 'link-register',
            ),
                )
        );

        $admin_bar->add_menu(array(
            'id' => 'login',
            'parent' => 'top-secondary',
            'title' => 'Faça Login',
            'href' => wp_login_url($redirect),
            'meta' => array(
                'class' => 'link-login',
            ),
                )
        );
    endif;
    return $admin_bar;
}

add_action('admin_bar_menu', 'alterar_admin_bar', 99);
add_filter('show_admin_bar', '__return_false');


/* -------------Menu Início----------------- */

// This theme uses wp_nav_menu() in one location.
register_nav_menus(array(
    'top-menu' => esc_html__('Menu Topo'),
    'rodape-menu' => esc_html__('Menu Rodapé'),
));

class BS3_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * Traverse elements to create list from elements.
     *
     * Display one element if the element doesn't have any children otherwise,
     * display the element and its children. Will only traverse up to the max
     * depth and no ignore elements under that depth. It is possible to set the
     * max depth to include all depths, see walk() method.
     *
     * This method shouldn't be called directly, use the walk() method instead.
     *
     * @since 2.5.0
     *
     * @param object $element Data object
     * @param array $children_elements List of elements to continue traversing.
     * @param int $max_depth Max depth to traverse.
     * @param int $depth Depth of current element.
     * @param array $args
     * @param string $output Passed by reference. Used to append additional content.
     * @return null Null on failure with no changes to parameters.
     */
    function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output) {
        $id_field = $this->db_fields['id'];

        if (isset($args[0]) && is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * @see Walker::start_el()
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item Menu item data object.
     * @param int $depth Depth of menu item. Used for padding.
     * @param int $current_page Menu item ID.
     * @param object $args
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        if ($args->has_children) {
            $item->classes[] = 'submenu';
        }


        if (is_object($args) && !empty($args->has_children)) {
            $link_after = $args->link_after;
            $args->link_after = ' <b class="caret"></b>';
        }

        parent::start_el($output, $item, $depth, $args, $id);

        if (is_object($args) && !empty($args->has_children)) {
            $args->link_after = $link_after;
        }
    }

    /**
     * @see Walker::start_lvl()
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of page. Used for padding.
     */
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\" role=\"menu\">\n";
    }

}

//function custom_wp_nav_menu($var) {
//    return is_array($var) ? array_intersect($var, array(
//                //List of allowed menu classes
//                'current_page_item',
//                'current_page_parent',
//                'current_page_ancestor',
//                'first',
//                'last',
//                'vertical',
//                'horizontal'
//                    )
//            ) : '';
//}
//
//add_filter('nav_menu_css_class', 'custom_wp_nav_menu');
//add_filter('nav_menu_item_id', 'custom_wp_nav_menu');
//add_filter('page_css_class', 'custom_wp_nav_menu');
//Deletes empty classes and removes the sub menu class
function strip_empty_classes($menu) {
    $menu = preg_replace('/ class=""| class="sub-menu"/', '', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'strip_empty_classes');

//Filtering a Class in Navigation Menu Item
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'current-page-active ';
    }
    $classes[] = 'normal_menu';
    $classes[] = 'mobile_menu_toggle';
    return $classes;
}

//resumo blog
function string_limit_words($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if (count($words) > $word_limit) {
        array_pop($words);
        return implode(' ', $words) . "...";
    } else {
        return implode(' ', $words);
    }
}

//Esconder API e contato
function wp_hide_actions_pages($query) {

    if (!is_admin()) {
        return $query;
    }
    global $pagenow, $post_type;

    if (!current_user_can('administrator') && $pagenow == 'edit.php' && $post_type == 'page') {
        $query->query_vars['post__not_in'] = array(get_page_by_path('contato'), get_page_by_path('api'));
    }
}

add_filter('parse_query', 'wp_hide_actions_pages');
