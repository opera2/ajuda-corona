<?php

function add_meta_tags() {
    $images = get_field('imagem');
    ?>
    <meta property="og:image" content="<?php echo $images['url'] ?>"/>
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="<?php echo $images['width'] ?>">
    <meta property="og:image:height" content="<?php echo $images['height'] ?>">
    <?php
}

add_action('wp_head', 'add_meta_tags');
get_header();
the_post();
?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4 mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <div class="text-center">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-primary" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Compartilhar no Facebook"><i class="fab fa-facebook"></i> Compartilhar no Facebook</a>
                            <a class="btn btn-success" href="whatsapp://send?text=<?php the_permalink(); ?>" data-action="share/whatsapp/share" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Compartilhar no WhatsApp"><i class="fab fa-whatsapp"></i> Compartilhar no WhatsApp</a>
                        </div>
                        <hr>
                        <?php if (get_field('video')) { ?>
                            <div class="embed-container">
                                <?php the_field('video'); ?>
                            </div>
                        <?php } else { ?>
                        <img src="<?php echo get_field('imagem')['url']; ?>" title="<?php the_title(); ?>" class="img-fluid">
                        <?php } ?>
                        <hr>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-primary" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Compartilhar no Facebook"><i class="fab fa-facebook"></i> Compartilhar no Facebook</a>
                            <a class="btn btn-success" href="whatsapp://send?text=<?php the_permalink(); ?>" data-action="share/whatsapp/share" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Compartilhar no WhatsApp"><i class="fab fa-whatsapp"></i> Compartilhar no WhatsApp</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>