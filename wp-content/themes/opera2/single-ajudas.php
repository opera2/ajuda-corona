<?php

function wpdocs_scripts_method_page() {
    //Javascript Template
    wp_enqueue_script('maps.googleapis.js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC2034wrxhRFDr9IMKbGcD_dPIbY-fdzCM', '', '', true);
    wp_enqueue_script('maps.js', get_stylesheet_directory_uri() . '/assets/js/maps.js', '', '', true);
}

add_action('wp_enqueue_scripts', 'wpdocs_scripts_method_page');
get_header();
?>
<div role="main" class="main mb-5">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <h4 class="m-0"><?php the_title(); ?></h4>
                    <?php the_field('detalhes_da_ajuda') ?>

                    <form method="GET">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-map-marker-alt"></i></span>
                            </div>
                            <input type="text" class="form-control" name="cidade" placeholder="Bairro, Cidade - UF" aria-label="Bairro, Cidade - UF" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit" id="button-addon2">Pesquisar</button>
                            </div>
                        </div>
                    </form>

                    <div class="google-map-borders">
                        <div id="googlemapsMarkers" class="google-map mt-0 mb-4" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>
<script>
    // Markers
    $("#googlemapsMarkers").gMap({
        controls: {
            draggable: (($.browser.mobile) ? false : true),
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },
        scrollwheel: false,
        markers: [
<?php
$voluntarios = new WP_Query(array('post_type' => 'voluntarios', 'posts_per_page' => '-1'));
if ($voluntarios->have_posts()) {
    while ($voluntarios->have_posts()) {
        $voluntarios->the_post();
        ?>
                    {
                        latitude: <?php the_field('latitude', 'user_' . $post->post_author) ?>,
                        longitude: <?php the_field('longitude', 'user_' . $post->post_author) ?>,
                        html: "<strong><?php the_title() ?></strong> - <a href='<?php echo get_permalink() ?>'><i class='fa fa-info'></i> Detalhes</a>",
                        icon: {
                            image: "<?php echo get_template_directory_uri(); ?>/assets/img/pin.png",
                            iconsize: [26, 46],
                            iconanchor: [12, 46]
                        }
                    },
        <?php
    }
    wp_reset_postdata();
}
$cidade = filter_input(INPUT_GET, 'cidade');
if (empty($cidade)) {
    $cidade = 'São Paulo - SP';
}
?>
        ],
        address: "<?php echo $cidade ?>",
        zoom: 11
    });
    var mapRef = $('#googlemapsStyle').data('gMap.reference');
    // Styles from https://snazzymaps.com/
    var styles = [{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 21}]}, {"elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}];
    var styledMap = new google.maps.StyledMapType(styles, {
        name: 'Styled Map'
    });
    mapRef.mapTypes.set('map_style', styledMap);
    mapRef.setMapTypeId('map_style');

</script>