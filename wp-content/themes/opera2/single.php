<?php get_header(); ?>
<div role="main" class="main">
    
    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>