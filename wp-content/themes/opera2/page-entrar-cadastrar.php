<?php

if (is_user_logged_in()) {
    wp_redirect(get_permalink(get_page_by_path('dashboard')));
}

get_header();
the_post();
?>

<div class="modal fade" id="recuperarSenha" tabindex="-1" role="dialog" aria-labelledby="recuperarSenha" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Recuperar senha</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_email">Informe seu e-mail para receber o link de recuperação de senha.</label>
                        <input type="email" class="form-control" id="user_email" name="user_email"aria-describedby="xxx" placeholder="Informe seu e-mail">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-success">Solicitar recuperação</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div role="main" class="main mb-5">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col">

                <div class="featured-boxes">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Entrar</h4>
                                    <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form">
                                        <div class="form-group">
                                            <label for="user_login">E-mail <span class="color-red">*</span></label>
                                            <input type="text" class="form-control" name="user_login" id="user_login" aria-describedby="user_login" placeholder="Informe seu e-mail">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_pass">Senha <span class="color-red">*</span></label>
                                            <input type="password" class="form-control" name="user_pass" id="user_pass" aria-describedby="user_pass" placeholder="Informe sua senha">
                                        </div>
                                        <div class="ps-login-btn">
                                            <input type="hidden" name="action" value="user_login">
                                            <input type="hidden" name="url" value="<?php echo get_permalink(get_page_by_path('perfil')) ?>">
                                            <button type="submit" class="btn btn-success">Entrar</button>
                                            <a href="#" data-toggle="modal" data-target="#recuperarSenha">Recuperar senha</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="featured-box featured-box-primary text-left mt-5">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Criar conta</h4>
                                    <form method="POST" action="<?php echo get_permalink(get_page_by_path('api')) ?>" class="ajax_form">
                                        <div class="form-group">
                                            <label for="first_name">Nome completo <span class="color-red">*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" aria-describedby="first_name" placeholder="Informe seu nome completo">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_email">E-mail <span class="color-red">*</span></label>
                                            <input type="email" class="form-control" id="user_email" name="user_email"aria-describedby="user_email" placeholder="Informe seu e-mail">
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="user_pass1">Senha <span class="color-red">*</span></label>
                                                    <input type="password" class="form-control" id="user_pass1" name="user_pass1"aria-describedby="user_pass1" placeholder="Informe uma senha">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="user_pass2">Repetir senha <span class="color-red">*</span></label>
                                                    <input type="password" class="form-control" id="user_pass2" name="user_pass2"aria-describedby="user_pass2" placeholder="Repita sua senha">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ps-login-btn">
                                            <input type="hidden" name="action" value="create_user_custom">
                                            <input type="hidden" name="url" value="<?php echo home_url() ?>">
                                            <button type="submit" class="btn btn-success">Cadastrar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<?php get_footer() ?>