<?php get_header(); ?>
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-light overlay-show overlay-op-10 mt-0" data-video-path="<?php echo get_template_directory_uri(); ?>/assets/video/coronavirus.mp4" data-plugin-video-background data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%'}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8"><?php the_title(); ?></h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-dark d-block text-center">
                        <li><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="active"><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4 mb-5">

        <div class="row">
            <div class="col">
                <div class="blog-posts">
                    <?php the_field('descricao') ?>
                    <?php
                    $images = get_field('imagens');
                    if ($images) {
                        ?>
                        <div class="masonry-loader masonry-loader-showing">
                            <div class="masonry lightbox" data-plugin-masonry  data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}" data-plugin-options="{'itemSelector': '.masonry-item'}">
                                <?php foreach ($images as $image) { ?>
                                    <div class="masonry-item">
                                        <a href="<?php echo $image['url'] ?>">
                                            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                                                <span class="thumb-info-wrapper">
                                                    <img src="<?php echo $image['sizes']['thumbnail'] ?>" class="img-fluid" alt="<?php echo $image['alt'] ?>">
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php include_once 'comments.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>