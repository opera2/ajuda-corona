$('.ajax_form').submit(function (e) {
    var form = $(this);
    var submit = form.find(".btn:submit");
    var text = submit.html();
    submit.prop('disabled', true);
    submit.html('<i class="fas fa-spinner fa-spin"></i> Aguarde...');
    $.ajax({
        dataType: "json",
        url: form.attr('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false
    }).done(function (data) {
        if (data.status == 'redirect' && data.showmessage == 'no') {
            window.location.replace(data.url);
        } else if (data.redirect == 'yes' && data.showmessage == 'yes') {
            $('#modalAjaxFormContent').html('<div class="alert alert-' + data.status + '" role="alert">' + data.message + '</div>');
            $('#modalAjaxForm').modal('show');

            $(document).on('hide.bs.modal', '#modalAjaxForm', function () {
                window.location.replace(data.url);
            });
        } else {
            $('#modalAjaxFormContent').html('<div class="alert alert-' + data.status + '" role="alert">' + data.message + '</div>');
            $('#modalAjaxForm').modal('show');
        }
    }).fail(function () {
        $('#modalAjaxFormContent').html('<div class="alert alert-danger" role="alert">Ocorreu um erro durante o envio, por favor, tente novamente mais tarde.</div>');
        $('#modalAjaxForm').modal('show');
    }).always(function (data) {
        submit.html(text);
        submit.prop('disabled', false);
        if (data.reset == 'yes') {
            form.trigger("reset");
        }
    });

    return false;
});

$(".ajudasbtn").change(function () {
    var id = $(this).data('ajudaid');
    $('#ajudasform' + id).submit();
});

$('.ajax_form_ajuda').submit(function (e) {
    var form = $(this);
    var id = form.data('ajudaid');
    $('#ajudaload' + id).html('<i class="fas fa-spinner fa-pulse color-green"></i>');
    $.ajax({
        dataType: "json",
        url: form.attr('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false
    }).done(function (data) {
        if (data.status == 'success') {
            $('#ajudaload' + id).html('<div class="text-' + data.status + '"><i class="fas fa-check-circle color-green"></i> ' + data.message + '</div>');
        } else {
            $('#ajudaload' + id).html('<i class="fas fa-times-circle color-red"></i>');
            if ($('#ajudabtn' + id).prop('checked')) {
                $('#ajudabtn' + id).bootstrapToggle('on', true);
            } else {
                $('#ajudabtn' + id).bootstrapToggle('off', true);
            }
            $('#ajudaload' + id).html('<div class="text-' + data.status + '"><i class="fas fa-times-circle color-red"></i> ' + data.message + '</div>');
        }
    }).fail(function () {
        $('#ajudaload' + id).html('<i class="fas fa-times-circle color-red"></i>');
        if ($('#ajudabtn' + id).prop('checked')) {
            $('#ajudabtn' + id).bootstrapToggle('on', true);
        } else {
            $('#ajudabtn' + id).bootstrapToggle('off', true);
        }
        $('#ajudaload' + id).html('<div class="text-' + data.status + '"><i class="fas fa-times-circle color-red"></i> ' + data.message + '</div>');
    }).always(function (data) {

    });

    return false;
});

$('#fotoperfil').change(function(){ $('.img_ajax').submit(); });