<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161715100-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-161715100-1');
        </script>


        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon.png" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-152x152.png" />
        <?php wp_head(); ?>

        <meta property="og:url" content="<?php echo home_url(); ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php bloginfo(); ?>" />
        <meta property="og:description" content="<?php bloginfo('description'); ?>" />
        <?php if (!is_single()) { ?>
            <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/combatecovid19.png" />
        <?php } ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="siteurl" data-url="<?php echo get_template_directory_uri(); ?>"></div>
        <div class="body">
            <header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
                <div class="header-body border-top-0 header-body-bottom-border">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-logo">
                                        <a href="<?php echo home_url() ?>">
                                            <img alt="<?php bloginfo(); ?>" width="82" height="40" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-default-slim.png">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-column justify-content-end">
                                <div class="header-row">
                                    <div class="header-nav header-nav-links order-2 order-lg-1">
                                        <div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                            <nav class="collapse">
                                                <?php
                                                if (is_user_logged_in()) {
                                                    wp_nav_menu(array(
                                                        'theme_location' => 'menu-principal-usuario',
                                                        'menu' => 'Menu Principal Usuário',
                                                        'items_wrap' => '<ul class="nav nav-pills" id="mainNav">%3$s</ul>',
                                                        'container' => NULL,
                                                        'walker' => new BS3_Walker_Nav_Menu,
                                                    ));
                                                } else {
                                                    wp_nav_menu(array(
                                                        'theme_location' => 'menu-principal-visitante',
                                                        'menu' => 'Menu Principal Visitante',
                                                        'items_wrap' => '<ul class="nav nav-pills" id="mainNav">%3$s</ul>',
                                                        'container' => NULL,
                                                        'walker' => new BS3_Walker_Nav_Menu,
                                                    ));
                                                }
                                                ?>
                                            </nav>
                                        </div>
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                    </div>
                                    <!--                                    <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                                                                            <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                                                                <a href="#" class="header-nav-features-toggle" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
                                                                                <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed" id="headerTopSearchDropdown">
                                                                                    <form role="search" action="<?php echo get_post_type_archive_link('voluntarios'); ?>" method="get">
                                                                                    <div class="simple-search input-group">
                                                                                        <input class="form-control text-1" id="headerSearch" name="s" type="search" value="" placeholder="Pesquisar...">
                                                                                        <span class="input-group-append">
                                                                                            <button class="btn" type="submit"><i class="fa fa-search header-nav-top-icon"></i></button>
                                                                                        </span>
                                                                                        <input type="hidden" name="post_type" value="voluntarios">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>